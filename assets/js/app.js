$(function() {

  heureDyn();
  
  function aff(result) {
    myP = document.getElementById('myP');
    myP.innerHTML = result;
  }
  
  /*
  * Affiche l'heure dynamiquement
  * @returns str
  */
 function heureDyn() {
   setInterval(function() {
     var theTime = new Date(),
     heure = theTime.getHours(),
     minute = theTime.getMinutes(),
     seconde = theTime.getSeconds();
     affHeure.innerHTML = pL0(heure) + ':' + pL0(minute) + ':' + pL0(seconde);
    }, 1000);
    
    function pL0(v) { // Première Lettre 0 si <10
      return (v > 9) ? v : '0' + v;
    }
  }
  
  console.log('Ready.');

});