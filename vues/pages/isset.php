<h2>Test // isset()

<div class="enBlanc">
<pre>
<?php

var_dump(isset($_SESSION['auth']));

$_SESSION['auth'] = '';
var_dump(isset($_SESSION['auth']));

$_SESSION['auth'] = 1234;
var_dump(isset($_SESSION['auth']));

?>
</pre>
</div>


