<p>Une page pour tests divers et rapides</p>
<hr>
Créer un formulaire contenant au minimum un champ de saisie de texte. Attention de ne pas oublier les attributs nécessaire au bon fonctionnement d'un formulaire method, action et name.
<hr>
<form action="/?page=form/traitement" method="post">

  <div class="form-group">
    <label for="name">Nom</label>
    <input type="text" class="form-control" name="name" id="name" aria-describedby="helpId" placeholder="Votre nom">
    <small id="helpId" class="form-text text-muted">Aide</small>
  </div>

  Sexe : 
  <div class="form-check form-check-inline">
    <input class="form-check-input" type="radio" name = "sexe" id="h" value="h">
    <label class="form-check-label">H</label>
  </div>
  <div class="form-check form-check-inline">
    <input class="form-check-input" type="radio" name = "sexe" id="f" value="f">
    <label class="form-check-label">F</label>
  </div>
  
  <div class="form-group">
    <label for="choix">Choix (Plusieurs possibles)</label>
    <select class="form-control" name="choix[]" id="choix" multiple>
      <option value="choix1">Choix 1</option>
      <option value="choix2">Choix 2</option>
      <option value="choix3">Choix 3</option>
      <option value="choix4">Choix 4</option>
      <option value="choix5">Choix 5</option>
      <option value="choix6">Choix 6</option>
      <option value="choix7">Choix 7</option>
    </select>
    <input type="hidden" name="page" value="form/traitement">
  </div>
  <div class="input-group">
      <button class="btn btn-primary" type="submit" aria-label="">Soumettre le formulaire</button>
  </div>
</form>
