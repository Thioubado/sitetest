<p>Une page pour tests divers et rapides</p>
<hr>
<p>Exemple de calculs réalisés en jQuery:
<p class="lionel">Calcule du sommiel et factoriel de <span id="valeur">n</span> en cours...<br>
Et du nombre de combinaisons du Loto (6 numéros parmis 49)...</p>
<hr>
<img src="assets\img\montagne-dvt-soleil.jpg" alt="Image du soleil caché par la montagne" width="100px"/>
<link rel="stylesheet" href="assets/css/infiny.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/98/three.min.js"></script>
<hr>
<div id="infiny"></div>
<script src="assets/js/infiny.js"></script>