<p>Le contenu de notre page d'accueil</p>
<hr>
Un exemple de... <span id="testDyn"></span>

<script>
    jq(function(){ // function pour exécuter du jQ n'importe où, sans se soucier du pré-chargement du DOM ni du jQ !

      $("#testDyn").html("Texte dynamique<br><em>(Inséré en jQ dont le code est directement dans le fichier HTML \"</em><strong>accueil.php</strong><em>\")</em>.");

    });
</script>

<?php
// Description de la page (Facultative)
$description = 'Exemple de page d\'accueil';
