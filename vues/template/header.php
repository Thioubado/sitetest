  <head>
    <meta charset="utf-8">
    <title>Sitetest</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/gif" href="/favicon.ico">

    <?php
      include "assets/css/all.php";
    ?>
    
    <script>
      function jq(fn){var d=document;(d.readyState=='loading')?d.addEventListener('DOMContentLoaded',fn):fn();}
    </script>
  </head>
