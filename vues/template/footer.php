<?php
include 'assets/js/all.php';
?>
<footer>
  <div class="row ">
    <div class="col">
      <button id="affHeure">HeureDyn</button>
      <span id="affDate"><?=date('d/m/Y')?></span>
    </div>
    <div class="col text-center etroit">
      <span><?=$_SERVER['SERVER_ADDR']?></span>
    </div>  
    <div class="col">
      &copy; 2018 - c57.fr
    </div>  
  </div>
</footer>