<?php
ob_start();
  include 'accueil.php';
ob_end_flush();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Sitetest</title>
    <meta content="">
    <link rel="icon" type="image/gif" href="/favicon.ico">
  </head>
  <body>
    <section class="container-fluid">

      <?php
        include 'css/all.php';
        ?>

      <?php
        include 'header.php';
        include 'menu.php';

      ?>
    </section>
    <?php
      include 'js/all.php';
    ?>
  </body>
</html>
